/* util.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
using GLib;

namespace Jellybean {
    public class JellybeanUtil : Object {
        [Description (nick = "Backend object that stores the jellybeans")]
        Jellybean.Jellybeans[] jellybeans_obj = {};

        construct {
            this.parse ();
        }


        [Description (nick = "Fill in internal jellybean object", blurb = "Parse Jellybean formatted string to Jellybean.Jellybeans[] object")]
        private void parse () {
            // string format inside array: "Item 1,; 50,; Items,; 10,; 2"
            // string[] argm = { "", "", "", "", ""};
            Jellybean.Jellybeans jb;
            jellybeans_obj = {};
            string[] parse_args = settings.get_string ("jellybeans").split ("~;");
            debug ("Parsing jellybean-formatted string: " + settings.get_string ("jellybeans"));

            for (int i = 0; i < parse_args.length; i++) {
                string[] argm = parse_args[i].split (",;");
                if (argm.length == 5) {
                    jb = new Jellybean.Jellybeans.from_props (
                                                              desanitize (argm[0]),
                                                              double.parse (argm[1]),
                                                              desanitize (argm[2]),
                                                              double.parse (argm[3]),
                                                              double.parse (argm[4]),
                                                              0
                    );
                } else {
                    jb = new Jellybean.Jellybeans.from_props (
                                                              desanitize (argm[0]),
                                                              double.parse (argm[1]),
                                                              desanitize (argm[2]),
                                                              double.parse (argm[3]),
                                                              double.parse (argm[4]),
                                                              int.parse (argm[5])
                    );
                }
                jellybeans_obj += jb;
            }
        }

        [Description (nick = "Add or edit a jellybean", blurb = "Add or edit a jellybean at index. If index is -1, jellybean is a new jellybean, else it is editing a jellybean")]
        public void add_item (int index = -1, Jellybean.Jellybeans jellybean = new Jellybean.Jellybeans ())
        requires (index > -2) {
            if (index == -1)
                jellybeans_obj += jellybean;
            else
                jellybeans_obj[index] = jellybean;

            jellybean_update ();
        }

        [Description (nick = "Delete a jellybean", blurb = "Deletes a jellybean at int index")]
        public void delete_item (int index = 0)
        requires (index > -1) {
            if ((jellybeans_obj.length - 1) == index)
                jellybeans_obj.move (index + 1, index, 1);
            else {
                for (int j = index; j < jellybeans_obj.length; j++)
                    jellybeans_obj.move (j + 1, j, 1);
            }
            jellybeans_obj.resize (jellybeans_obj.length - 1);
            jellybean_update ();
        }

        [Description (nick = "Returns Jellybeans[] object", blurb = "returns whole jellybeans object, for t.ex. constructing the rows")]
        public Jellybean.Jellybeans[] jellybeans_object () {
            return jellybeans_obj;
        }

        [Description (nick = "Compiles a jellybean object into a storable string")]
        private string jellybean_compile (Jellybean.Jellybeans jlb) {
            jlb = jlb ?? new Jellybean.Jellybeans ();
            string name = sanitize (jlb.name);
            string unit = sanitize (jlb.unit);
            string num = jlb.number.to_string ();
            string lo = jlb.low.to_string ();
            string qui = jlb.quick.to_string ();
            string icon = ((int) jlb.icon).to_string ();
            return @"$name,;$num,;$unit,;$lo,;$qui,;$icon";
        }

        [Description (nick = "Updates the stored string with the current value of the Jellybeans[] array")]
        public void jellybean_update () {
            string retur = jellybean_compile (jellybeans_obj[0]);

            if (jellybeans_obj.length > 1) {
                for (int i = 1; i < jellybeans_obj.length; i++) {
                    retur += "~;";
                    retur += jellybean_compile (jellybeans_obj[i]);
                }
            }

            settings.set_string ("jellybeans", retur);
            settings.apply ();
            debug ("Updated saved jellybean-format string: " + settings.get_string ("jellybeans"));
        }

        [Description (nick = "Updates list of jellybeans", blurb = "Updates list of jellybeans stored in mv_lb and binds show_func to each list item")]
        public void update_rows (Gtk.ListBox mv_lb) {         // memory handling issues here with over 6 jellybeans
            Jellybean.Row[] ? rows = {};
            bool[] is_priority = {};
            int i = 0;

            for (i = 0; i < jellybeans_obj.length; i++) {
                rows += new Jellybean.Row.from_jellybean (i, jellybeans_obj[i]);
                is_priority += (jellybeans_obj[i].number <= jellybeans_obj[i].low);
            }

            mv_lb.remove_all ();
            for (i = 0; i < jellybeans_obj.length; i++) {
                if (is_priority[i] && prioritize_ls)
                    mv_lb.prepend (rows[i]);
                else
                    mv_lb.append (rows[i]);
            }
            rows = null;
        }

        private string sanitize (string str) {
            return str.replace (";", "&;&").replace (",", "&,&").replace ("~", "&~&");
        }

        private string desanitize (string str) {
            return str.replace ("&;&", ";").replace ("&,&", ",").replace ("&~&", "~");
        }
    }
}
