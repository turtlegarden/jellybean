using Gtk 4.0;
using Adw 1;

Adjustment nmadjustment {
  lower: 0;
  upper: 2147483647;
  step-increment: 1;
  value: 50;
}

Adjustment lsadjustment {
  lower: 0;
  upper: 1000;
  step-increment: 1;
  value: 10;
}

Adjustment quadjustment {
  lower: 0;
  upper: 100;
  step-increment: 1;
  value: 2;
}

template $JellybeanWindow: Adw.ApplicationWindow {
  height-request: 360;
  width-request: 300;
  default-height: 460;
  default-width: 360;
  title: _("Stockpile");

  content: Adw.ToastOverlay to {
    Adw.NavigationView nv {
      Adw.NavigationPage {
        title: _("Stockpile");
        tag: "mv";

        child: Adw.ToolbarView mv {
          [top]
          Adw.HeaderBar hb1 {
            [start]
            ToggleButton mv_search {
              icon-name: "loupe-symbolic";
              tooltip-text: _("Search");
              active: bind search_bar.search-mode-enabled;
            }

            [start]
            Button mv_add {
              icon-name: "plus-large-symbolic";
              action-name: "win.add-jellybean";
              tooltip-text: _("New Item");
            }

            [end]
            MenuButton mv_menu {
              icon-name: "open-menu-symbolic";
              primary: true;
              menu-model: menu;
              tooltip-text: _("Main Menu");
            }
          }

          [top]
          SearchBar search_bar {
            search-mode-enabled: bind mv_search.active;
            key-capture-widget: mv;

            SearchEntry search_entry {}
          }

          ScrolledWindow {
            Adw.Clamp mv_adwc {
              maximum-size: 600;
              tightening-threshold: 375;
              margin-top: 12;
              margin-bottom: 12;
              margin-start: 12;
              margin-end: 12;
              orientation: horizontal;

              ListBox mv_lb {
                vexpand: false;
                valign: start;
                selection-mode: none;

                styles [
                  "boxed-list"
                ]

                Adw.ActionRow {
                  title: "Nothing here";
                  action-name: "navigation.push";
                  action-target: '"sv"';
                }

                [placeholder]
                Adw.StatusPage mv_status_page {
                  title: _("No Results Found");
                  icon-name: "loupe-regular-symbolic";
                  description: _("Try a different search term");
                }
              }
            }
          }
        };
      }

      Adw.NavigationPage iv {
        title: "Item 1";
        tag: "iv";

        child: Adw.ToolbarView {
          [top]
          Adw.HeaderBar hb2 {
            show-title: false;

            [end]
            Button hb2_eb {
              icon-name: "edit-symbolic";
              action-name: "win.edit-jellybean";
              tooltip-text: _("Edit Item");
            }
          }

          Adw.StatusPage sp1_sp {
            title: "Item 1";
            description: "487 left";
            margin-bottom: 24;

            Box {
              halign: center;
              spacing: 6;

              Button sp1_sp_drain {
                label: _("Use");
                action-name: "win.use-jellybean";

                styles [
                  "suggested-action",
                  "pill"
                ]
              }

              Button sp1_sp_refill {
                label: _("Refill");
                action-name: "win.refill-jellybean";

                styles [
                  "pill"
                ]
              }
            }
          }
        };
      }

      Adw.NavigationPage ev {
        title: "Editing New Item";
        tag: "ev";

        child: Adw.ToolbarView {
          [top]
          Adw.HeaderBar hb3 {
            show-back-button: false;
            show-end-title-buttons: false;

            [start]
            Button hb3_cb {
              label: _("Cancel");
              action-name: "navigation.pop";
            }

            [end]
            Button hb3_sb {
              label: _("Save");
              action-name: "win.save-jellybean";

              styles [
                "suggested-action"
              ]
            }
          }

          content: ScrolledWindow {
            Adw.Clamp {
              maximum-size: 600;
              tightening-threshold: 375;
              margin-top: 12;
              margin-bottom: 12;
              margin-start: 12;
              margin-end: 12;
              orientation: horizontal;

              Gtk.Box {
                orientation: vertical;

                ListBox {
                  vexpand: false;
                  valign: start;
                  selection-mode: none;

                  styles [
                    "boxed-list"
                  ]

                  Adw.EntryRow ev_title {
                    title: _("Item Name");
                    input-hints: emoji;
                    text: _("New Item");
                  }

                  Adw.ActionRow ev_icon {
                    title: _("Item Icon");

                    [suffix]
                    MenuButton ev_icon_button {
                      styles [
                        "circular"
                      ]

                      vexpand: false;
                      valign: center;
                      icon-name: "bread-symbolic";
                      popover: icon_popover;
                    }
                  }

                  Adw.SpinRow ev_number {
                    title: _("Item Amount");
                    climb-rate: 10;
                    adjustment: nmadjustment;
                  }

                  Adw.EntryRow ev_unit {
                    title: _("Item Units");
                    text: _("Units");
                  }

                  Adw.SpinRow ev_lowstock {
                    title: _("Low Stock Number");
                    climb-rate: 10;
                    adjustment: lsadjustment;
                  }

                  Adw.SpinRow ev_quickuse {
                    title: _("Default Usage Amount");
                    climb-rate: 10;
                    adjustment: quadjustment;
                  }
                }

                Button ev_db {
                  label: _("Delete Item…");
                  margin-top: 12;
                  hexpand: false;
                  halign: start;
                  action-name: "win.delete-jellybean";

                  styles [
                    "destructive-action"
                  ]
                }
              }
            }
          };
        };
      }
    }
  };
}

Popover icon_popover {
  Box popover_box {
    spacing: 6;
    orientation: vertical;
  }
}

menu menu {
  section {
    item {
      label: _("_Preferences");
      action: "win.preferences";
    }

    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("_About Stockpile");
      action: "app.about";
    }
  }
}
