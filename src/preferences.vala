/* preferences.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace Jellybean {
    [GtkTemplate (ui = "/garden/turtle/Jellybean/preferences.ui")]
    public class PreferencesWindow : Adw.PreferencesWindow {
        [GtkChild] public  unowned Adw.SwitchRow priority_low_stock;
        [GtkChild] private unowned Adw.SwitchRow dim_icons;

        public PreferencesWindow (Jellybean.Window win) {
            Object (
                transient_for: win
            );
        }

        construct {
            settings.bind(
                "priority-low-stock",
                this.priority_low_stock,
                "active",
                0
            );
            settings.bind(
                "dim-icons",
                this.dim_icons,
                "active",
                0
            );
        }
    }
}
