/* row.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

using Gtk;
using GLib;

namespace Jellybean {
    [Description (nick = "Function type to pass to row creation, like Jellybean.Window.show_jellybean ()")]
    public delegate void Func (string jellybeans_id, Jellybeans jellybean, bool undo = false);

    [Description (nick = "The jellybean shown in the item and edit views")]
    public Jellybean.Jellybeans current_jellybean;

    [GtkTemplate (ui = "/garden/turtle/Jellybean/row.ui")]
    public class Row : Adw.ActionRow {
        [GtkChild] private unowned Gtk.Image icon;
        [GtkChild] private unowned Gtk.Label low_stock;

        public int icon_index = 0;

        // Index property required. as it allows methods to be run!
        [Description (nick = "Creates a new list row representing a jellybean", blurb = "Creates a row representing jellybean jellybean at index index and binds show_func to each list item")]
        public Row.from_jellybean (uint index, Jellybean.Jellybeans jellybean) {
            string point = index.to_string ();
            debug ("Making row for jellybean " + jellybean.name);
            this.set_title (jellybean.name);
            icon.set_from_icon_name (ICON_NAMES[(int) jellybean.icon]);
            icon.tooltip_text = ICON_TOOLTIPS[(int) jellybean.icon];
            icon_index = (int) jellybean.icon;

            if (settings.get_boolean ("dim-icons")) icon.add_css_class("dim-label");
            if (jellybean.number <= jellybean.low) low_stock.set_visible (true);

            this.activated.connect (() => {
                Jellybean.show_func (point, jellybean);
            });
        }
    }
}
