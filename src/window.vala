/* window.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either verutilon 3 of the License, or
 * (at your option) any later verutilon.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace Jellybean {                                               // Widget documentation
    [GtkTemplate (ui = "/garden/turtle/Jellybean/main.ui")]         //
    public class Window : Adw.ApplicationWindow {                   // General
        [GtkChild] private unowned Adw.ToastOverlay to;             // Toast overlay for application
        [GtkChild] private unowned Adw.NavigationView nv;           // Navigation view for application
        [GtkChild] private unowned Adw.NavigationPage iv;           // Item view navigation page. (For editing properties)
        [GtkChild] private unowned Adw.NavigationPage ev;           // Edit view navigation page. (For editing properties)
                                                                    //
                                                                    // Main View
        [GtkChild] private unowned Adw.Clamp mv_adwc;               // Main view clamp (for showing No Results status page)
        [GtkChild] private unowned Adw.StatusPage mv_status_page;   //           The aforementioned No Results status page
        [GtkChild] public  unowned Gtk.ListBox mv_lb;               // The main list box for showing items
        [GtkChild] private unowned Gtk.SearchEntry search_entry;    // Search bar
        [GtkChild] private unowned Gtk.SearchBar search_bar;        // Search bar parent, that hides and shows it
        [GtkChild] private unowned Gtk.MenuButton mv_menu;          // Main Menu button
                                                                    //
                                                                    // Item View
        [GtkChild] private unowned Adw.StatusPage sp1_sp;           // Item view status page
        [GtkChild] private unowned Gtk.Button sp1_sp_drain;         // “Use…” button (for controlling sensitivity)
                                                                    //
                                                                    // Edit View
        [GtkChild] private unowned Adw.EntryRow ev_title;           // Title                    (Edit mode)
        [GtkChild] private unowned Gtk.MenuButton ev_icon_button;   // Icon selector button     (Edit mode)
        [GtkChild] private unowned Gtk.Adjustment nmadjustment;     // Current number           (Edit mode)
        [GtkChild] private unowned Adw.EntryRow ev_unit;            // Unit                     (Edit mode)
        [GtkChild] private unowned Gtk.Adjustment quadjustment;     // Standard usage amount    (Edit mode)
        [GtkChild] private unowned Gtk.Adjustment lsadjustment;     // Low stock amount         (Edit mode)
        [GtkChild] private unowned Gtk.Button ev_db;                // Delete button            (Edit mode)
        [GtkChild] private unowned Gtk.Box popover_box;             // Box for containing icon buttons shown on the icon selector button


        [Description (nick = "Function that runs backend management of jellybeans")]
        private Jellybean.JellybeanUtil util = new Jellybean.JellybeanUtil ();
        [Description (nick = "Int to store the current jellybean's index")]
        private int jellybean_index;
        [Description (nick = "Marks whether the edit window is closed while being saved")]
        private bool is_save = false;
        [Description (nick = "Marks whether the edit window is creating a new jellybean")]
        private bool is_new_jellybean = false;
        uint8 use_status = 0;

        private Jellybean.StringSearchTerms string_search_terms = new Jellybean.StringSearchTerms ();
        private bool is_showing_results = false;

        public Gtk.Application app { get; construct; }

        // Define functions

        public Window (Gtk.Application app) {
            Object (
                    application: app,
                    app: app
            );
        }

        construct {
            ActionEntry[] action_entries = {
                { "discard-dialog", () => {
                    if (nv.get_visible_page ().tag != "ev") return;
                    nv.pop_to_tag ("iv");
                }},
                { "preferences", this.on_preferences},
                { "save-jellybean", this.save_jellybean },
                { "use-jellybean", this.use_jellybean },
                { "refill-jellybean", this.refill_jellybean },
                { "add-jellybean", this.add_jellybean },
                { "edit-jellybean", this.edit_jellybean },
                { "delete-jellybean", this.delete_jellybean },
                { "main-menu", mv_menu.activate_default },
            };
            this.add_action_entries (action_entries, this);

            // Since gettext does not work without an init outside a window, we will do this here.
            ICON_TOOLTIPS = {
                _("Ticket"), _("Paper"), _("Bread"), _("Cafe"), _("Medicine"), _("Wallet")
            };

            // Set accelerators
            app.set_accels_for_action ("win.add-jellybean", { "<Primary>n" });
            app.set_accels_for_action ("win.discard-dialog", { "<Primary>l" });
            app.set_accels_for_action ("win.save-jellybean", { "<Primary>s" });
            app.set_accels_for_action ("win.use-jellybean", { "<Primary>u" });
            app.set_accels_for_action ("win.refill-jellybean", { "<Primary>r" });
            app.set_accels_for_action ("win.edit-jellybean", { "<Primary>e" });
            app.set_accels_for_action ("win.delete-jellybean", { "<Primary>d" });
            app.set_accels_for_action ("win.main-menu", { "F10" });
            app.set_accels_for_action ("win.preferences", { "<Primary>comma" });
            app.set_accels_for_action ("win.show-help-overlay", { "<Ctrl>question" });

            Jellybean.show_func = this.show_jellybean;

            prioritize_ls = settings.get_boolean ("priority-low-stock");

            util.update_rows (mv_lb);
            ev.hidden.connect (discard_dialog);

            mv_lb.set_filter_func (filter_row);
            search_entry.search_changed.connect (() => {
                if (!is_showing_results) mv_adwc.set_child (mv_lb);
                is_showing_results = false;
                mv_lb.invalidate_filter ();
                if (!is_showing_results && search_bar.get_search_mode ()) mv_adwc.set_child (mv_status_page);
            });

            Gtk.Builder ui_builder = new Gtk.Builder ();
            try {
                ui_builder.add_from_resource ("/garden/turtle/Jellybean/help-overlay.ui");
                this.set_help_overlay (ui_builder.get_object ("help_overlay") as Gtk.ShortcutsWindow);
            } catch (Error e) {
                critical ("Could not load help overlay: " + e.message);
            }

            populate_icon_popover ();

            if (Config.PROFILE == "Development") this.add_css_class ("devel");
        }

        private void on_preferences () {
            PreferencesWindow pref_window = new Jellybean.PreferencesWindow (this);
            pref_window.close_request.connect(() => {
                settings.apply ();
                prioritize_ls = pref_window.priority_low_stock.active;
                util.update_rows (mv_lb);
                return false;
            });
            pref_window.present ();
        }

        [Description (nick = "Saves changes in edit view")]
        private void save_jellybean () {
            if (nv.get_visible_page ().tag != "ev") return;

            Jellybean.Jellybeans jellybean = new Jellybean.Jellybeans.from_props (
                                                                                  ev_title.get_text (),
                                                                                  nmadjustment.get_value (),
                                                                                  ev_unit.get_text (),
                                                                                  lsadjustment.get_value (),
                                                                                  quadjustment.get_value (),
                                                                                  int.parse (ev_icon_button.get_name ())
            );
            Jellybean.current_jellybean = jellybean;
            util.add_item (jellybean_index, jellybean);
            util.jellybean_update ();

            util.update_rows (mv_lb);
            if (jellybean_index == -1) {
                Adw.Toast toast = new Adw.Toast (_("Item saved as %s").printf (jellybean.name));
                toast.timeout = 5;
                to.add_toast (toast);
                is_save = true;
                nv.pop_to_tag ("mv");
                return;
            }

            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("Low stock: @d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());
            else
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());

            this.sp1_sp.set_description (infostr);
            this.sp1_sp.set_title (jellybean.name);

            bool is_sensitive = jellybean.number != 0;
            debug (is_sensitive.to_string ());
            this.sp1_sp_drain.set_sensitive (is_sensitive);

            is_save = true;
            is_new_jellybean = false;

            Adw.Toast toast = new Adw.Toast (_("Item updated as %s").printf (jellybean.name));
            toast.timeout = 3;
            to.add_toast (toast);

            nv.pop_to_tag ("iv");
            return;
        }

        [Description (nick = "Handles leaving the edit view. Is not actually a dialog")]
        private void discard_dialog () {
            if (!is_save) {
                Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;
                bool was_new_jellybean = is_new_jellybean;
                Adw.Toast toast = new Adw.Toast (!is_new_jellybean ?
                                                 _("Changes to %s discarded").printf (jellybean.name) :
                                                 _("Changes to item discarded"));
                toast.set_button_label (_("Undo"));
                toast.button_clicked.connect (() => {
                    nv.push_by_tag ("ev");
                    if (was_new_jellybean) is_new_jellybean = true;
                });
                toast.timeout = 6;
                to.add_toast (toast);
                is_new_jellybean = false;
            }
        }

        [Description (nick = "Opens item view for a jellybean: bound to a row")]
        private void show_jellybean (string jellybeans_id, Jellybeans jellybean, bool undo = false) { // bool undo is not used, it keeps consistency with func type
            uint j = uint.parse (jellybeans_id ?? "0");

            this.sp1_sp.set_title (jellybean.name);
            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("Low stock: @d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());
            else
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());

            this.sp1_sp.set_description (infostr);

            jellybean_index = (int) j;
            Jellybean.current_jellybean = jellybean;

            bool is_sensitive = jellybean.number != 0;
            debug (is_sensitive.to_string ());
            this.sp1_sp_drain.set_sensitive (is_sensitive);

            if (is_new_jellybean && !is_save) {
                this.nv.pop_to_tag ("iv");
                return;
            }

            is_new_jellybean = false;
            this.nv.push_by_tag ("iv");
        }

        // "Uses" a selected item
        private void use_jellybean () {
            if (nv.get_visible_page ().tag != "iv") return;
            Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;

            /*
            Adw.MessageDialog msg = new Adw.MessageDialog (
                                                           this,
                                                           // Translators: %s is the jellybean's name
                                                           _("Use %s").printf (jellybean.name),
                                                           // Translators: %s of @s → units of Item
                                                           _("How many %s of @s would you like to use?").printf (jellybean.unit).replace ("@s", jellybean.name)
            );
            msg.close_response = "cancel";
            msg.add_response ("cancel", _("Cancel"));
            // Translators: Imperative for using changes
            msg.add_response ("use", _("Use"));
            msg.set_response_appearance ("use", Adw.ResponseAppearance.SUGGESTED);

            double num;
            num = jellybean.number + 1;
            if (jellybean.number <= 1) num = jellybean.number;
            if (jellybean.number == 0) return;

            Gtk.Adjustment adj = new Gtk.Adjustment (jellybean.quick, 1, num, 1, 10, 1);
            Gtk.SpinButton spb = new Gtk.SpinButton (adj, 10, 0);
            msg.set_extra_child (spb);

            msg.response.connect ((response) => {
                if (response == "cancel") return;
                else { */


            if (jellybean.number < jellybean.quick) {
                // Translators: %s → jellybean unit
                Adw.Toast toast = new Adw.Toast (_("Not enough %s left").printf (jellybean.unit));
                toast.timeout = 3;
                this.to.add_toast (toast);
                return;
            }

            jellybean.number -= jellybean.quick;
            Adw.Toast toast = new Adw.Toast(""); // Must assign variable to not throw error


            if (use_status != 2) {
                // Translators: @d %s of @s → {number} units of Item
                toast = new Adw.Toast (_("Used @d %s of @s").printf (jellybean.unit).replace ("@d",
                    jellybean.quick.to_string ()
                ).replace ("@s", jellybean.name));
                toast.set_priority (HIGH);
                toast.timeout = 3;
                // toast.set_button_label (_("Undo"));
                // toast.button_clicked.connect (() => {
                //     use_status = 1;
                //     this.refill_jellybean ();
                // });
            }

            use_status = 0;

            this.util.add_item (jellybean_index, jellybean);

            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("Low stock: @d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());
            else
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());

            this.sp1_sp.set_description (infostr);
            util.update_rows (mv_lb);

            if (jellybean.number == 0) sp1_sp_drain.set_sensitive (false);

            if (use_status != 2) {
                Adw.Toast? deltoast = this.to.get_child () as Adw.Toast;
                if (deltoast != null) deltoast.dismiss ();
                if (toast.title != "") this.to.add_toast (toast);
            }
            use_status = 0;
            return;

            /*
                }
            });

            msg.present ();
            */
        }

        // "Refills" a focused item
        private void refill_jellybean () {
            if (nv.get_visible_page ().tag != "iv") return;
            Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;

            /*
            Adw.MessageDialog msg = new Adw.MessageDialog (
                                                           this,
                                                           // Translators: %s is the jellybean's name
                                                           _("Refill %s").printf (jellybean.name),
                                                           // Translators: %s of @s → units of Item
                                                           _("How many %s of @s would you like to add?").printf (jellybean.unit).replace ("@s", jellybean.name)
            );
            msg.close_response = "cancel";
            msg.add_response ("cancel", _("Cancel"));
            // Translators: Imperative for adding changes to jellybean
            msg.add_response ("add", _("Add"));
            msg.set_response_appearance ("add", Adw.ResponseAppearance.SUGGESTED);

            Gtk.Adjustment adj = new Gtk.Adjustment (jellybean.quick, 1, 2147483647 - jellybean.number, 1, 10, 1);
            Gtk.SpinButton spb = new Gtk.SpinButton (adj, 10, 0);
            msg.set_extra_child (spb);

            msg.response.connect ((response) => {
                if (response == "cancel") return;
                else { */
            jellybean.number += jellybean.quick;
            Adw.Toast toast = new Adw.Toast(""); // Must assign variable to not throw error

            if (use_status != 1) {
                // Translators: @d %s to @s → {number} units to Item
                toast = new Adw.Toast (_("Added @d %s to @s").printf (jellybean.unit).replace ("@d",
                                                                                               jellybean.quick.to_string ()
                ).replace ("@s", jellybean.name));
                toast.set_priority (HIGH);
                toast.timeout = 3;
                /// toast.set_button_label (_("Undo"));
                // toast.button_clicked.connect (() => {
                //     use_status = 2;
                //     this.use_jellybean ();
                // });
            }

            util.add_item (jellybean_index, jellybean);

            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("Low stock: @d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());
            else
                // Translators:@d %s → for example, 23 units or 495 units
                infostr = _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ());

            this.sp1_sp.set_description (infostr);
            util.update_rows (mv_lb);

            if ((sp1_sp_drain.get_sensitive () == false) && (jellybean.number > 0)) sp1_sp_drain.set_sensitive (true);

            if (use_status != 1) {
                Adw.Toast? deltoast = this.to.get_child () as Adw.Toast;
                if (deltoast != null) deltoast.dismiss ();
                if (toast.title != "") this.to.add_toast (toast);
            }
            use_status = 0;
            return;

            /*
                }
            });

            msg.present ();
            */
        }

        // Opens the New Item window
        private void add_jellybean () {
            if (nv.get_visible_page ().tag != "mv") return;
            ev_title.text = _("New Item");
            nmadjustment.set_value (50);
            ev_unit.text = _("Units");
            lsadjustment.set_value (10);
            quadjustment.set_value (2);
            this.ev_icon_button.icon_name = ICON_NAMES[0];
            this.ev_icon_button.set_name ("0");
            ev.set_title (_("Editing %s").printf (_("New Item")));
            ev_db.set_visible (false);
            is_new_jellybean = true;
            is_save = false;

            nv.push_by_tag ("ev");
            jellybean_index = -1;
        }

        private void edit_jellybean () {
            if (nv.get_visible_page ().tag != "iv") return;
            Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;
            int icon = (int) jellybean.icon;
            ev_title.text = jellybean.name;
            nmadjustment.set_value (jellybean.number);
            ev_unit.text = jellybean.unit;
            lsadjustment.set_value (jellybean.low);
            quadjustment.set_value (jellybean.quick);
            ev_icon_button.icon_name = ICON_NAMES[icon];
            ev_icon_button.tooltip_text = ICON_TOOLTIPS[icon];
            ev_icon_button.set_name (icon.to_string ());
            iv.set_title (jellybean.name);
            ev.set_title (_("Editing %s").printf (jellybean.name));
            ev_db.set_visible (util.jellybeans_object ().length != 1);
            nv.push_by_tag ("ev");
            is_save = false;
        }

        private void delete_jellybean () {
            if (nv.get_visible_page ().tag != "ev") return;
            if (ev_db.get_visible () == false) return;

            Adw.MessageDialog msg = new Adw.MessageDialog (
                                                           this,
                                                           _("Delete Item?"),
                                                           _("If you delete this item, its information will be deleted permanently.")
            );
            msg.close_response = "cancel";

            msg.add_response ("cancel", _("Cancel"));
            // Translators: Imperative for discarding changes
            msg.add_response ("discard", _("Delete"));
            msg.set_response_appearance ("discard", Adw.ResponseAppearance.DESTRUCTIVE);

            msg.response.connect ((response) => {
                if (response == "cancel") return;
                else {
                    util.delete_item (jellybean_index);
                    util.update_rows (mv_lb);
                    is_save = true;
                    nv.pop_to_tag ("mv");
                    Adw.Toast toast = new Adw.Toast (_("Item deleted"));
                    toast.timeout = 3;
                    to.add_toast (toast);
                    return;
                }
            });

            msg.present ();
        }

        private void populate_icon_popover () {
            Jellybean.IconButton? icon_button = new Jellybean.IconButton (0);
            Gtk.Box[] box = new Gtk.Box[2];
            int jenum = 0;
            int ienum;
            do {
                box[jenum] = new Gtk.Box (HORIZONTAL, 6);
                ienum = 3 * jenum;
                do {
                    debug (ienum.to_string ());
                    icon_button.clicked.connect ((button) => {
                        int icon = int.parse (button.get_name ());
                        ev_icon_button.icon_name = ICON_NAMES[icon];
                        ev_icon_button.set_name (icon.to_string ());
                        ev_icon_button.tooltip_text = ICON_TOOLTIPS[icon];
                        ev_icon_button.get_popover ().popdown ();
                    });
                    box[jenum].append (icon_button);
                    ienum++;
                    icon_button = new Jellybean.IconButton (ienum);
                } while (ienum <= (3 * jenum) + 2);

                popover_box.append (box[jenum]);
                jenum++;
            } while (jenum <= 1);
        }

        private bool filter_row (Gtk.ListBoxRow row) {
            Jellybean.Row new_row = (Jellybean.Row) row;
            Regex regex;

            try {
                regex = new Regex (Regex.escape_string (search_entry.text), CASELESS, PARTIAL_SOFT);
            } catch (Error e) {
                critical (e.message);
                return false;
            }

            bool match = regex.match (new_row.title);
            if (!match) match = string_search_terms.match_with_regex (new_row.icon_index, regex);
            if (match) is_showing_results = true;

            return match;
        }
    }
}
