<img align="left" style="vertical-align: middle;" width="160" height="160" src="https://codeberg.org/turtle/stockpile/raw/branch/main/data/icons/hicolor/scalable/apps/garden.turtle.Jellybean.svg">

# Stockpile
Manage inventories of various things

[![Please do not theme this app](https://stopthemingmy.app/badge.svg)](https://stopthemingmy.app) [![Stockpile's translation status](https://translate.codeberg.org/widget/jellybean/jellybean/svg-badge.svg)](https://translate.codeberg.org/engage/jellybean) [![Total Stockpile installs](https://img.shields.io/badge/dynamic/json?color=informational&label=downloads&logo=flathub&logoColor=white&query=%24.installs_total&url=https%3A%2F%2Fflathub.org%2Fapi%2Fv2%2Fstats%2Fgarden.turtle.Jellybean)](https://flathub.org/apps/garden.turtle.Jellybean)

<p align="center"><img alt="The main view page of Stockpile" src="https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/main-view.png"/></p>
<p align="center"><a href='https://flathub.org/apps/garden.turtle.Jellybean'><img width='260' alt='Download Stockpile on Flathub' src='https://dl.flathub.org/assets/badges/flathub-badge-en.png'/></a></p>

## Building

Clone in GNOME Builder and run.

## Contributing

UI is coded in Blueprint, and formatting should be always default. If you update a blueprint file, remember to run `blueprint-compiler format -f *.blp` in the `src` directory. However, due to [weirdness regarding `valac` importing the GResources file](https://codeberg.org/turtle/stockpile/issues/4), `.ui` files must manually be compiled. To do this, run `blueprint-compiler batch-compile . . *.blp` in the `src` directory.

If you change a localized string, please regenerate all .POT and .PO files.
To do this, run `meson _build --prefix=/usr && meson compile -C _build stockpile-update-po` in a terminal, or select the build target `stockpile-update-po` (NOT `po/stockpile-update-po`!) in Builder and build. If you are on the latest version of Meson, this should work.

If you don't like codeberg, feel free to send patches to <stockpile@turtle.garden> (and any issue or programming things). No other forges (excepting GNOME GitLab) will be added.

The [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) is used in this project; please, make sure you are following the Code of Conduct when contributing to this project!

- **Be friendly.** Use welcoming and inclusive language.
- **Be empathetic.** Be respectful of differing viewpoints and experiences.
- **Be respectful.** When we disagree, we do so in a polite and constructive manner.
- **Be considerate.** Remember that decisions are often a difficult choice between competing priorities.
- **Be patient and generous.** If someone asks for help it is because they need it.
- **Try to be concise.** Read the discussion before commenting.


## Translation

Stockpile uses Weblate to manage translations! If wanted, you can request a specific file and send it back via a patch if you are not happy with creating a Codeberg account or using Weblate.

[![Stockpile translation status](https://translate.codeberg.org/widget/jellybean/jellybean/multi-auto.svg)](https://translate.codeberg.org/engage/jellybean/)
